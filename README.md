# ESERCITAZIONI PRATICHE DEL CORSO DI BIOINFORMATICA   

## Programma
#### Banche dati: definizioni generali e sistemi di retrieval
- [X] [Definizione di Database e struttura delle entry **GenBank**](Banche_dati/banche_dati.md)  
- [X] [Sistemi di retrieval](Banche_dati/retrieval.md)  
- [X] [PubMed](Banche_dati/pubmed.md)

#### Genome Browser 
- [X] [ENSEMBL](Genome_Browser/Ensembl.md)
- [X] [UCSC](Genome_Browser/UCSC.md)

#### Allineamento tra sequenze
- [X] [Allineamento](Allineamento_tra_sequenze/allineamento.md)
- [X] [Utilizzo di BLAST e BLAT per l'analisi genomica](Allineamento_tra_sequenze/blast_genomico.md)

#### Metabarcoding
- [ ] [Fastq & FASTQC](./Fastq/Accesso_al_server.md)