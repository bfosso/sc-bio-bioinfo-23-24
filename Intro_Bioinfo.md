# ***Introduzione alla Bioinformatica***

Prima di addentrarci nello studio di questa disciplina è opportuno soffermarsi su quale sia la definizione più 
appropriata per il termine ***Bioinformatica***, delinearne i principali ambiti di ricerca e applicazione.  
Nel corso degli anni sono state proposte numerose definizioni che possono essere così sintetizzate:  

***La Bioinformatica si occupa della ricerca, dello sviluppo e dell’applicazione di approcci computazionali per l’interpretazione di dati biologici.***  

**Questi includono l’informazione genetica racchiusa nei genomi e le modalità della loro espressione (trascrittoma, proteoma, microbioma, pathways metabolici, ecc.).**  

Tre sono gli ambiti di applicazione principali della Bioinformatica:  
***1. Raccolta, Conservazione e Distribuzione di dati biologici;***  
***2. Sviluppo di algoritmi.***  
***3. Utilizzo degli strumenti sviluppati per generare nuova conoscenza.***  

La Bioinformatica è per sua natura **multidisciplinare** richiedendo il concorso di competenze diverse 
(Biologia Molecolare, Biochimica, Fisiologia, Genetica, Informatica, Matematica, Statistica, Chimica, Fisica, Ingegneria
, ecc.) e ha applicazioni pratiche in vasti settori della scienza.   

L’aumento esponenziale dei dati biologici, cui assistiamo in questi anni, anche come conseguenza del completamento di numerosi 
progetti di sequenziamento genomico (incluso il genoma umano) e dell’avvio di numerosi progetti su larga scala per lo 
studio del trascrittoma, del proteoma e del microbioma di numerosi organismi, comporta una crescente importanza della 
Bioinformatica in tutti i settori della ricerca a livello biomolecolare. [https://www.ncbi.nlm.nih.gov/genbank/statistics/](https://www.ncbi.nlm.nih.gov/genbank/statistics/)
  
I principali tipi di dati biologici sono:  
    -	**Sequenze nucleotidiche** (vedi [https://www.ncbi.nlm.nih.gov/guide/dna-rna/](https://www.ncbi.nlm.nih.gov/guide/dna-rna/) per una lista delle risorse di sequenze di DNA o RNA oppure [http://www.ebi.ac.uk/ena](http://www.ebi.ac.uk/ena) per un archivio di informazioni sul sequenziamento nucleotidico nel mondo);  
    -	**Sequenze proteiche** (vedi [https://www.ncbi.nlm.nih.gov/guide/proteins/](https://www.ncbi.nlm.nih.gov/guide/proteins/) per un elenco di risorse proteiche oppure [http://us.expasy.org/sprot/](http://us.expasy.org/sprot/) per informazioni su una delle più importanti risorse di sequenze proteiche, la *UniProtKB/Swiss-Prot*);   
    -	**Dati Genomici** (vedi [ncbi.nlm.nih.gov/genome/](https://www.ncbi.nlm.nih.gov/genome) banca pubblica che collezione sequenze ed annotazioni dei genomi);   
    -	**Strutture tridimensionali** di proteine ed altre macromolecole (vedi [https://www.ncbi.nlm.nih.gov/guide/domains-structures/](https://www.ncbi.nlm.nih.gov/guide/domains-structures/) per le risorse di domini e strutture molecolari oppure [http://www.rcsb.org/pdb/](http://www.rcsb.org/pdb/) per la risorsa di macromolecole biologiche *PDB*);     
    -	**Dati di espressione** ([https://www.ncbi.nlm.nih.gov/guide/genes-expression/](https://www.ncbi.nlm.nih.gov/guide/genes-expression/) per le risorse relative all’espressione dei geni e ai fenotipi correlati);   
    -	**Dati metabolici** ([http://www.genome.jp/kegg/](http://www.genome.jp/kegg/) per una risorsa sulle relazioni metaboliche tra i geni e i loro prodotti);  
    -	**Letteratura scientifica** (per accedere ad un’importante risorsa di letteratura biomedica [https://www.ncbi.nlm.nih.gov/pubmed/](https://www.ncbi.nlm.nih.gov/pubmed/)).   

_Una buona conoscenza di base della __Biologia Molecolare__ e in generale delle discipline biologiche di base 
(e.g. *Fisiologia*, *Biochimica* e *Genetica*) è fondamentale per comprendere e utilizzare al meglio gli strumenti 
Bioinformatici che utilizzeremo nel corso delle esercitazioni._   

Lo sviluppo della Bioinformatica e in generale degli strumenti bioinformatici è stato affiancato da un enorme sviluppo 
della rete internet (o **world wide web**). Lo sviluppo della rete internet ha reso possibile la “globalizzazione” 
delle informazioni.  
E’ sufficiente avere un computer connesso alla rete per accedere, da una qualunque località, a tutte le informazioni 
archiviate e rese disponibili da un qualunque altro computer in rete sulla terra. 

Gli indirizzi internet sopra riportati (noti come *URL* o *uniform resource locators*) consentono l’accesso, 
utilizzando un Browser come *Safari*, *Chrome* o *Firefox* a documenti elettronici attraverso 
il protocollo *http/https* (*HyperText Transfer Protocol*).  
La maggior parte delle risorse bioinformatiche, sia banche dati che programmi di analisi, sono disponibili ai ricercatori
– normalmente in forma gratuita - sulla rete internet.   
  
Prima d'iniziare queste **Esercitazioni** è opportuno ricordare alcuni concetti base di biologia molecolare riguardanti:  
•	la struttura di un gene (promotore, ORF, esone, introne, siti di splicing, segnale di poliadenilazione, etc);  
•	la struttura di un trascritto (UTR, ORF, sequenza di _Shine e Dalgarno_, sito di poliadenilazione, etc);  
•	gli elementi di una proteina (peptide segnale, segmenti transmembrana, siti di fosforilazione, domini);   
•	le differenze di funzione e struttura e tra un gene e uno pseudogene;  
•	la differenza tra sequenze genomiche ed _Expressed Sequence Tag (EST)_.  
  
Letture consigliate:  
- Valle G. et al. Introduzione alla Bioinformatica (Zanichelli Ed.) – pag. 1-6.  
- Citterich M. et al. Fondamenti di Bioinformatica (Zanichelli)

[Definizione di Database e struttura delle entry **GenBank**](Banche_dati/banche_dati.md)  

[Programma Esercitazioni](README.md)